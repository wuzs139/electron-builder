// if (window.screen.width <= '750') {
//   (function (doc, win) {
//     var docEle = doc.documentElement,
//       dpr = Math.min(win.devicePixelRatio, 3);
//     console.log(dpr);
//     scale = 1 / dpr,
//       resizeEvent = 'orientationchange' in window ? 'orientationchange' : 'resize';
//     docEle.dataset.dpr = dpr;

//     var metaEle = doc.createElement('meta');
//     metaEle.name = 'viewport';
//     metaEle.content = 'width=375,initial-scale=' + scale + ',maximum-scale=' + scale + ',user-scalable=yes,minimal-ui';
//     document.getElementsByTagName('head')[0].appendChild(metaEle);

//     // docEle.firstElementChild.appendChild(metaEle);
//     var recalCulate = function () {
//       var width = docEle.clientWidth;


//       if (width / dpr > 750) {
//         width = 750 * dpr;
//       }
//       docEle.style.fontSize = 69 * (width / 750) + 'px';
//       console.log(docEle.style.fontSize)


//     };
//     recalCulate();
//     if (!doc.addEventListener) return;
//     win.addEventListener(resizeEvent, recalCulate, false);

//     // console.log(dpr)
//   })(document, window);

// } else if (window.screen.width >= '1000') {
//   console.log('wde');
//   (function (doc, win) {
//     var docEle = doc.documentElement,
//       dpr = Math.min(win.devicePixelRatio, 3);
//     console.log(dpr);
//     scale = 1 / dpr,
//       resizeEvent = 'orientationchange' in window ? 'orientationchange' : 'resize';
//     docEle.dataset.dpr = dpr;
//     var metaEle = doc.createElement('meta');
//     metaEle.name = 'viewport';
//     metaEle.content = 'width=device-width,initial-scale=' + scale + ',maximum-scale=' + scale + ',user-scalable=no,minimal-ui';
//     docEle.firstElementChild.appendChild(metaEle);
//     var recalCulate = function () {
//       var width = docEle.clientWidth;
//       if (width / dpr > 1080) {
//         width = 1080 * dpr;
//       }
//       docEle.style.fontSize = 100 * (width / 1080) + 'px';
//     };
//     recalCulate();
//     if (!doc.addEventListener) return;
//     win.addEventListener(resizeEvent, recalCulate, false);
//   })(document, window);
// }

























if (window.screen.width <= '750') {

  (function (designWidth, maxWidth) {
    console.log('wd750')

    var doc = document,
      win = window,
      docEl = doc.documentElement,
      remStyle = document.createElement("style"),
      tid;

    function refreshRem() {
      var width = docEl.getBoundingClientRect().width;
      maxWidth = maxWidth || 540;
      width > maxWidth && (width = maxWidth);
      var rem = width *69 / designWidth;
      // var rem = width *90 / designWidth;

      remStyle.innerHTML = 'html{font-size:' + rem + 'px;}';
    }

    if (docEl.firstElementChild) {
      docEl.firstElementChild.appendChild(remStyle);
    } else {
      var wrap = doc.createElement("div");
      wrap.appendChild(remStyle);
      doc.write(wrap.innerHTML);
      wrap = null;
    }
    //要等 wiewport 设置好后才能执行 refreshRem，不然 refreshRem 会执行2次；
    refreshRem();

    win.addEventListener("resize", function () {
      clearTimeout(tid); //防止执行两次
      tid = setTimeout(refreshRem, 300);
    }, false);

    win.addEventListener("pageshow", function (e) {
      if (e.persisted) { // 浏览器后退的时候重新计算
        clearTimeout(tid);
        tid = setTimeout(refreshRem, 300);
      }
    }, false);
    if (doc.readyState === "complete") {
      doc.body.style.fontSize = "12px";
    } else {
      doc.addEventListener("DOMContentLoaded", function (e) {
        doc.body.style.fontSize = "12px";
      }, false);
    }


  })(750, 750);

} else if (window.screen.width >= '1000') {

  (function (designWidth, maxWidth) {
    console.log('wd1080')
    var doc = document,
      win = window,
      docEl = doc.documentElement,
      remStyle = document.createElement("style"),
      tid;

    function refreshRem() {
      var width = docEl.getBoundingClientRect().width;
      maxWidth = maxWidth || 540;
      width > maxWidth && (width = maxWidth);
      var rem = width * 100 / designWidth;
      remStyle.innerHTML = 'html{font-size:' + rem + 'px;}';
    }

    if (docEl.firstElementChild) {
      docEl.firstElementChild.appendChild(remStyle);
    } else {
      var wrap = doc.createElement("div");
      wrap.appendChild(remStyle);
      doc.write(wrap.innerHTML);
      wrap = null;
    }
    //要等 wiewport 设置好后才能执行 refreshRem，不然 refreshRem 会执行2次；
    refreshRem();

    win.addEventListener("resize", function () {
      clearTimeout(tid); //防止执行两次
      tid = setTimeout(refreshRem, 300);
    }, false);

    win.addEventListener("pageshow", function (e) {
      if (e.persisted) { // 浏览器后退的时候重新计算
        clearTimeout(tid);
        tid = setTimeout(refreshRem, 300);
      }
    }, false);
    if (doc.readyState === "complete") {
      doc.body.style.fontSize = "12px";
    } else {
      doc.addEventListener("DOMContentLoaded", function (e) {
        doc.body.style.fontSize = "12px";
      }, false);
    }


  })(1080, 1080);

}
