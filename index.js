const {app, BrowserWindow} = require('electron');

let win;
function createWindow () {

    // 创建浏览器窗口
    win = new BrowserWindow({
      fullscreen : true,
      //width: 1080,
      //height:1920,
      frame : false,
      /*webPreferences : {
        webSecurity: false,
        zoomFactor : 0.3
      },*/
      icon : 'www/logo.png'
    });
  
    // win.setThumbarButtons([{
    //     tooltip : '开发者工具',
    //     click : function(){
    //       // 打开开发者工具
    //       win.webContents.openDevTools();
    //     }
    //   }]);

    // 然后加载应用的 index.html。
    win.loadFile('www/index.html');
    
    // 当 window 被关闭，这个事件会被触发。
    win.on('closed', () => {
      // 取消引用 window 对象，如果你的应用支持多窗口的话，
      // 通常会把多个 window 对象存放在一个数组里面，
      // 与此同时，你应该删除相应的元素。
      win = null
    });

    // 打开开发者工具
    //win.webContents.openDevTools();

  }
  
// 限制只可以打开一个应用, 4.x的文档
	const gotTheLock = app.requestSingleInstanceLock()
	if (!gotTheLock) {
	  app.quit()
	   return;
	} else {
	  app.on('second-instance', (event, commandLine, workingDirectory) => {
		// 当运行第二个实例时,将会聚焦到mainWindow这个窗口
		if (win) {
		  if (win.isMinimized()) win.restore()
		  win.focus()
		  win.show()
		}
	  })
	   // Electron 会在初始化后并准备
	  // 创建浏览器窗口时，调用这个函数。
	  // 部分 API 在 ready 事件触发后才能使用。
	  app.on('ready', createWindow);
	}

	
	
	
  

  // 当全部窗口关闭时退出。
  app.on('window-all-closed', () => {
    // 在 macOS 上，除非用户用 Cmd + Q 确定地退出，
    // 否则绝大部分应用及其菜单栏会保持激活。
    if (process.platform !== 'darwin') {
      app.quit()
    }
  })
  
  
  app.on('activate', () => {
    // 在macOS上，当单击dock图标并且没有其他窗口打开时，
    // 通常在应用程序中重新创建一个窗口。
    if (win === null) {
      createWindow()
    }
  })


app.setLoginItemSettings({
  openAtLogin: true, // Boolean 在登录时启动应用
  openAsHidden: true, // Boolean (可选) mac 表示以隐藏的方式启动应用。~~~~
  // path: '', String (可选) Windows - 在登录时启动的可执行文件。默认为 process.execPath.
  // args: [] String Windows - 要传递给可执行文件的命令行参数。默认为空数组。注意用引号将路径换行。
})



/*const Menu = remote.Menu;
const MenuItem = remote.MenuItem;

var menu = new Menu();
menu.append(new MenuItem({ 
    label: '开发者工具', 
    click: function() { 
       
    }
}));
menu.append(new MenuItem({ type: 'separator' }));

window.addEventListener('contextmenu', function (e) {
  e.preventDefault();
  menu.popup(remote.getCurrentWindow());
}, false);*/

  // 在这个文件中，你可以续写应用剩下主进程代码。
  // 也可以拆分成几个文件，然后用 require 导入。